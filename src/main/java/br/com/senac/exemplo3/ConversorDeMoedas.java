package br.com.senac.exemplo3;

public class ConversorDeMoedas {

    private final double COTACAO_DOLAR = 4.09;
    private final double COTACAO_EURO = 4.79;
    private final double COTACAO_AUSDOLAR = 2.95;

    public double CalcularRealParaDollar(double valor) {
        double dolar = valor / COTACAO_DOLAR;

        return dolar;
    }

    public double CalcularRealParaEuro(double valor) {
        double euro = valor / COTACAO_EURO;

        return euro;
    }

    public double CalcularRealParaDolarAUS(double valor) {
        double dolarAus = valor / COTACAO_AUSDOLAR;

        return dolarAus;
    }
}
