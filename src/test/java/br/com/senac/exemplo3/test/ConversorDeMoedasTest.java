package br.com.senac.exemplo3.test;

import br.com.senac.exemplo3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;

public class ConversorDeMoedasTest {

    public ConversorDeMoedasTest() {
    }

    @Test
    public void ResultadoDollar() {
        ConversorDeMoedas dolar = new ConversorDeMoedas();
        double real = 10;
        double resultado = dolar.CalcularRealParaDollar(real);
        assertEquals(2.45, resultado, 0.05);
    }

    @Test
    public void ResultadoEuro() {
        ConversorDeMoedas euro = new ConversorDeMoedas();
        double real = 15;

        double resultado = euro.CalcularRealParaEuro(real);

        assertEquals(3.13, resultado, 0.05);

    }

    @Test
    public void ResultadoDollarAUS() {
        ConversorDeMoedas dolaraus = new ConversorDeMoedas();
        double real = 15;

        double resultado = dolaraus.CalcularRealParaDolarAUS(real);

        assertEquals(5.08, resultado, 0.05);

    }

}
